import java.util.ArrayList;
import java.util.Scanner;

public class Benutzerverwaltung {
	
	private static ArrayList<Benutzer> benutzerliste = new ArrayList<Benutzer>();

	public static void main(String[] args) {
		
		
		int auswahl;
		
		do {
			auswahl = menu();
			switch (auswahl) {
			case 1:
				benutzerAnzeigen();
				// benutzerAnzeigen();
				break;
			case 2:
				benutzerErfassen();
				// bentzerErfassen();
				break;
			case 3:
				benutzerLöschen();
				// benutzerLöschen();
				break;
			case 4:
				System.exit(0);
			default:
				System.err.println("\nFalsche Eingabe.\n");				
			}
		}while (true );
		
	}
	
	public static void benutzerAnzeigen() {
		
		for (int i=0 ; i < benutzerliste.size(); i++) {
			System.out.println(benutzerliste.get(i).toString());
		}
	}
	
	public static void benutzerErfassen() {

        Benutzer b = new Benutzer("", 0);

        System.out.println("Name: ");
        Scanner s = new Scanner(System.in);
        String name = s.next();
        b.setName(name);

        System.out.println("Benutzernummer: ");
        int benutzernummer = s.nextInt();
        b.setBenutzernummer(benutzernummer);

        benutzerliste.add(new Benutzer (name, benutzernummer));
	}
	
	
	public static void benutzerLöschen() {
	
	System.out.println("Aktuelles Benutzerverzeichnis: \n ");
    for (int i=0 ; i < benutzerliste.size(); i++) {
        System.out.println(benutzerliste.get(i).toString());
    }
    
    System.out.println("\nGeben sie die Indexnummer an die gelöscht werden soll. \n" + "Indexnummer beginnt bei '0'! \n" + "Indexnummer: ");
    Scanner s = new Scanner(System.in);
    int eintrag = s.nextInt();
    
    if (eintrag != benutzerliste.size()) {
        benutzerliste.remove(eintrag);
        System.out.println("Eintrag erfolgreich gelöscht!\n");
    } else {
        System.out.println("Eintrag exisitiert nicht, versuchen Sie eine andere Nummer.\n");
    }
}
	
	
	public static int menu() {

        int selection;
        Scanner input = new Scanner(System.in);

        /***************************************************/

        System.out.println("     Benutzerverwaltung     ");
        System.out.println("----------------------------\n");
        System.out.println("1 - Benutzer anzeigen");
        System.out.println("2 - Benutzer erfassen");
        System.out.println("3 - Benutzer löschen");
        System.out.println("4 - Ende");
         
        System.out.print("\nEingabe:  ");
        selection = input.nextInt();
        return selection;    
    }

}
