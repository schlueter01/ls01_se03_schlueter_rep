import java.util.Scanner;

public class Vorschleife {

	
		
		public static void main(String[] args) {
			
			Scanner myScanner = new Scanner(System.in);
			
			System.out.println("Geben Sie bitte n ein: ");
			
			int n = myScanner.nextInt();

//   While Schleife			
			
//			int zaehler = 1;
			
//			while ( zaehler <= n) {
//				System.out.print(zaehler + " ");
//				zaehler++;
//			}

			
//    Vorschleife (wenn wir wissen wie oft es laufen soll)			
			
//			for (int zaehler = 1 ; zaehler <= n ; zaehler++ ) {
//				System.out.print(zaehler + " ");
			
			
//		}

		for (int zaehler = 1 ; zaehler <= n ; zaehler++ ) {
			System.out.print(zaehler + " + ");
		
		}
	
}

}