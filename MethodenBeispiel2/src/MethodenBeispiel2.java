
public class MethodenBeispiel2 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		add(3,4);
		
		let(5,5);
		int erg2 = let(5,5);
		System.out.println("Ergebnis : "+ erg2);
	}

	public static void add(int zahl1, int zahl2) {
		int erg = zahl1 + zahl2;
		
		System.out.println(zahl1 +" + " + zahl2 + " = "+ erg);
		System.out.println(erg + " + " +zahl1);
	}
	
	public static int let(int zahl1, int zahl2) {
		int erg2 = zahl1 + zahl2;
		
		return erg2;
		
	}
}
