
public class MethodeBeispiel {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

	//sayHello();		
		
	//sayHello("Benni");
	
	 //add(2,3);	
	 
	  // add(3.5,7.7);
	
		//summe(3,6,7); 
		
		berechnePreis(5,10.50,"Monitor");
		
	}

	public static void add(int zahl1, int zahl2 ) {
		
		int erg = zahl1 + zahl2;
		System.out.println(zahl1 + "+"+zahl2 + " = "+ erg);
	}
	
	public static void add(double zahl1, double zahl2) {
		
		double erg = zahl1 + zahl2; 
		System.out.println(zahl1 + "+"+zahl2 + " = "+ erg);
	}
	
	public static void summe(int zahl1, int zahl2, int zahl3) {
		
		int erg = zahl1 + zahl2 + zahl3; 
		System.out.println(zahl1 + "+"+zahl2 + "+"+zahl3 + " = "+ erg);
	}
	
	public static void berechnePreis(int zahl1, double zahl2, String name) {
		
		double erg = zahl1 * zahl2;
		System.out.println(zahl1 + " "+ name + "e kosten " + erg + "0 Euro"); 
	}
	
	public static void sayHello() {
		
		
		System.out.println("hello...");
	}
	
    public static void sayHello(String name) {
		
		
		System.out.println("hello "+name);
	}
}
