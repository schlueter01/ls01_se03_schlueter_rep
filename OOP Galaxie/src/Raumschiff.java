import java.util.ArrayList;

public class Raumschiff {
	
	private int photonentorpedoAnzahl;
	private int energieversorgungInProzent;
	private int schildeInProzent;
	private int huelleInProzent;
	private int lebenserhaltungssystemeInProzent;
	private String schiffsname;
	private int androidenAnzahl;
	
	private ArrayList<Ladung> ladungsverzeichnis = new ArrayList<Ladung>();
	
	public Raumschiff() {
		this.photonentorpedoAnzahl = 0;
		this.energieversorgungInProzent = 0;
		this.schildeInProzent = 0;
		this.huelleInProzent = 0;
		this.lebenserhaltungssystemeInProzent = 0;
		this.schiffsname = "Unbekannt";
		this.androidenAnzahl = 0;
		
	}

	public Raumschiff(int photonentorpedoAnzahl, int androidenAnzahl, int energieversorgungInProzent,
			int schildeInProzent, int huelleInProzent, int lebenserhaltungssystemeInProzent, String schiffsname) {
		
		this.photonentorpedoAnzahl = photonentorpedoAnzahl;
		this.energieversorgungInProzent = energieversorgungInProzent;
		this.schildeInProzent = schildeInProzent;
		this.huelleInProzent = huelleInProzent;
		this.lebenserhaltungssystemeInProzent = lebenserhaltungssystemeInProzent;
		this.schiffsname = schiffsname;
		this.androidenAnzahl = androidenAnzahl;
	}
	
	public int getPhotonentorpedoAnzahl() {
		return this.photonentorpedoAnzahl;
	}

	public void setPhotonentorpedoAnzahl(int photonentorpedoAnzahl) {
		this.photonentorpedoAnzahl = photonentorpedoAnzahl;
	}
	
	public int getEnergieversorgungInProzent() {
		return this.energieversorgungInProzent;
	}

	public void setEnergieversorgungInProzent(int energieversorgungInProzent) {
		this.energieversorgungInProzent = energieversorgungInProzent;
	}
	
	public int getSchildeInProzent() {
		return this.schildeInProzent;
	}

	public void setSchildeInProzent(int schildeInProzent) {
		this.schildeInProzent = schildeInProzent;
	}
	
	public int getHuelleInProzent() {
		return this.huelleInProzent;
	}

	public void setHuelleInProzent(int huelleInProzent) {
		this.huelleInProzent = huelleInProzent;
	}
	
	public int getLebenserhaltungssystemeInProzent() {
		return this.lebenserhaltungssystemeInProzent;
	}

	public void setLebenserhaltungssystemeInProzent(int lebenserhaltungssystemeInProzent) {
		this.lebenserhaltungssystemeInProzent = lebenserhaltungssystemeInProzent;
	}
	
	public String getSchiffsname() {
		return this.schiffsname;
	}

	public void setSchiffsname(String schiffsname) {
		this.schiffsname = schiffsname;
	}
	
	public int getAndroidenAnzahl() {
		return this.androidenAnzahl;
	}

	public void setAndroidenAnzahl(int androidenAnzahl) {
		this.androidenAnzahl = androidenAnzahl;
	}
	
	public void addLadung (Ladung neueLadung) {
		ladungsverzeichnis.add(neueLadung);
	}
	
	public void ladungsverzeichnisAusgeben () {
		for (int i=0 ; i < ladungsverzeichnis.size(); i++) {
            System.out.println(ladungsverzeichnis.get(i).toString());
            
		}	
	}
	
	public void zustandRaumschiff() {
        System.out.println("\n" + "Raumschiff: " + getSchiffsname());
        System.out.println("Energieversorgung: " + getEnergieversorgungInProzent());
        System.out.println("Schilde: " + getHuelleInProzent());
        System.out.println("H�lle: " + getHuelleInProzent());
        System.out.println("Lebenserhaltungssysteme: " + getLebenserhaltungssystemeInProzent());
        System.out.println("Androidenanzahl: " + getAndroidenAnzahl());
        System.out.println("Photonentorpedoanzahl: " + getPhotonentorpedoAnzahl());
    }
	
	public void nachrichtAnAlle(String message) {
		System.out.println(message);
	}
	
	private void treffer(Raumschiff r) {
		System.out.println("[" + r.toString() + "] wurde getroffen!\n");
	}
	
	public String toString() {
		return this.schiffsname;
	}
	
	public void photonentorpedoSchiessen(Raumschiff r) {
		
		if (photonentorpedoAnzahl == 0) {
			r.nachrichtAnAlle("-=Click=-");		
		} else {
			photonentorpedoAnzahl--;
			r.nachrichtAnAlle("Photonentorpedo abgeschossen");
			r.treffer(r);
		}
		 
		
	}
	
	public void phaserkanoneSchiessen (Raumschiff r) {

        if (energieversorgungInProzent < 50) {
            r.nachrichtAnAlle("-=Click=-");
        } else {
            energieversorgungInProzent = energieversorgungInProzent - 50;
            r.nachrichtAnAlle("Phaserkanone abgeschossen");
            r.treffer(r);
        }
    }
}
